import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';


describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'pruebas'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('pruebas');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('pruebas app is running!');
  });

  it('test de titulo', () => {

    const fixture = TestBed.createComponent(AppComponent);
    const h1:HTMLElement= fixture.nativeElement.querySelector('h1');
    console.log('h1',h1);
    console.log('contenido:',h1.textContent);

    console.log('contenido titulo del componente:',h1.textContent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    console.log('contenido titulo del componente:',(compiled.querySelector('.content span').textContent))
    expect(compiled.querySelector('.content span').textContent).toContain('pruebas app is running!');
  });
});

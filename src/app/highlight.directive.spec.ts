import { By } from '@angular/platform-browser'
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HighlightDirective } from './highlight.directive';

fdescribe('HighlightDirective', () => {
  let comp:AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let titleEl: DebugElement;

  beforeEach( () => {
     TestBed.configureTestingModule({
       declarations: [
        AppComponent,
        HighlightDirective,
      ],
    })
    fixture = TestBed.createComponent(AppComponent);
    comp = fixture.componentInstance;
    titleEl= fixture.debugElement.query(By.css('.claseTituloDeResources'));
  });

  it('onHover background must to be yellow', () => {
    titleEl.triggerEventHandler('mouseover',null);
    fixture.detectChanges();
    expect(titleEl.nativeElement.style.backgroundColor).toBe('yellow');
  });

  it('onLeave background must to be red', () => {
    titleEl.triggerEventHandler('mouseout',null);
    fixture.detectChanges();
    expect(titleEl.nativeElement.style.backgroundColor).toBe('red');
  });





  // it('should create an instance', () => {
  //   const directive = new HighlightDirective();
  //   expect(directive).toBeTruthy();
  // });
});

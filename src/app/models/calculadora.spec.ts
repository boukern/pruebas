import { Calculadora } from './calculadora';

fdescribe('Test class Calculadora:', () => {

  it('should create an instance', () => {
    expect(new Calculadora()).toBeTruthy();
  });

    describe('Test Calculadora.multiplicar:', () => {

      it('2 * 5 = 10', () => {
        let cal= new Calculadora();
        expect(cal.multiplicar(2,5)).toEqual(10);
      });

    });

    describe('Test Calculadora.dividir:', () => {

      it('21 / 7 = 3', () => {
        let cal= new Calculadora();
        expect(cal.dividir(21,7)).toEqual(3);
      });

      it('5 / 0 = null', () => {
        let cal= new Calculadora();
        expect(cal.dividir(5,0)).toEqual(null);
      });

    });

});

import { Person } from './person';
fdescribe('Test para PersonModel', () => {


  // describe('Test0: Vamos a probar los metodos de esta clase', () => {
  //   it('se debe crear una instancia de Person', () => {
  //     const person = new Person('Naoufel', 'Bouker', 49);
  //     expect(person).toBeTruthy();
  //     // expect(new Person()).toBeTruthy();
  //   });
  // });

  describe('Test1: metodo person.getFullName', () => {

    it('- Test1-1:debe devolver nombre + apellido', () => {
      // ARRANGE
      const person = new Person('Nicolas', 'Molina', 25);
      // ACT
      const rta = person.getFullName();
      // ASSERT
      expect(rta).toEqual('Nicolas-Molina');
    });

  });

  describe('Test2: metodo person.getAgeInYears', () => {

    // it('- Test 2-1 : tiene 49 debe devolver 59 anios', () => {
    //   // Arrange
    //   const person = new Person('Naoufel', 'Bouker', 49);
    //   // Act
    //   const age = person.getAgeInYears(10);
    //   // Assert
    //   expect(age).toEqual(59);
    // });

    it('- Test 2-2  : Sumando edad negativa devolvera la edad de entrada 49 anios', () => {
      // Arrange
      const person = new Person('Naoufel', 'Bouker', 49);
      // Act
      const age = person.getAgeInYears(-10);
      // Assert
      expect(age).toEqual(49);
    });


  });

});

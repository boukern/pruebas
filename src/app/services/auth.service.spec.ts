import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';


interface Post{
  userId:number;
  id:number;
  tigle:string;
  body:string
}

fdescribe('AuthService', () => {
  let service: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientModule]
    });
    service = TestBed.inject(AuthService);
  });

  it('recebir datos de forma correcta', (evaluarAlTerminar: DoneFn) => {
    service.getPost(2).subscribe((post:Post)=>{
      console.log('data ist',post);
      expect(post.id).toEqual(2);
      evaluarAlTerminar();
    });
  });
});

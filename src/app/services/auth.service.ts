import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';


interface Post{
  userId:number;
  id:number;
  tigle:string;
  body:string
}


@Injectable({
  providedIn: 'root'
})




export class AuthService {

  constructor(private http:HttpClient) { }

  getPost(postId:number):Observable<Post>{
    return this.http.get<Post>(`http://jsonplaceholder.typicode.com/posts/${postId}`)
  }

}

import { TitleCasePipe } from './title-case.pipe';

fdescribe('TitleCasePipe', () => {
  it('create an instance', () => {
    const pipe = new TitleCasePipe();
    expect(pipe.transform('hello')).toBe('HELLO');
  });
});
